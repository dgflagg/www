import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Welcome to the <code>Dead Mans Switch</code> utility.
        </p>
        <a
          className="App-link"
          href="https://en.wikipedia.org/wiki/Dead_man%27s_switch"
          target="_blank"
          rel="noopener noreferrer"
        >
          Dead Mans Switch
        </a>
      </header>
    </div>
  );
}

export default App;
