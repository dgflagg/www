import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';
import axios from 'axios';


export default class MessageDetails extends React.Component {
  state = {
    id: '',
    message: {}
  }

  handleChange = event => {
    this.setState({ id: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();

    axios.get(`http://dms.dgflagg.info:8080/message?id=${this.state.id}`)
      .then(res => {
        console.log(res);
        console.log(res.data);
        const message = res.data;
        this.setState({ message });
      })
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Message ID:
            <input type="text" name="id" onChange={this.handleChange}  />
          </label>
          <button type="submit">Get Message</button>
        </form>
        <ul>
          <li>ID: {this.state.message.id}</li>
          <li>Content Location: {this.state.message.contentLocation}</li>
          <li>Email: {this.state.message.email}</li>
          <li>Scheduled Delivery: {this.state.message.scheduledDelivery}</li>
        </ul>
      </div>
    )
  }
}

export class NewMessage extends React.Component {
  state = {
    content: '',
    email: '',
    scheduledDelivery: '',
  }

  handleInputChange = event => {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      this.setState({[name]: value    });
  }

  handleSubmit = event => {
    event.preventDefault();

    const message = {
      content: this.state.content,
      email: this.state.email,
      scheduledDelivery: this.state.scheduledDelivery
    };

    axios.post(`http://dms.dgflagg.info:8080/message`,
        "",
        { headers: {"content": `${this.state.content}`, "email": `${this.state.email}`, "scheduledDelivery": `${this.state.scheduledDelivery}`} }
    )
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Content:
            <input type="text" name="content" onChange={this.handleInputChange} /><br />
            Email:
            <input type="text" name="email" onChange={this.handleInputChange} /><br />
            Scheduled Delivery:
            <input type="text" name="scheduledDelivery" onChange={this.handleInputChange} /><br />
          </label>
          <button type="submit">Add</button>
        </form>
      </div>
    )
  }
}


ReactDOM.render(
  <React.StrictMode>
    <App />
    <MessageDetails />
    <NewMessage />
  </React.StrictMode>,
  document.getElementById('root')
);
