#!/bin/bash

# update the docker container locally
docker pull us.gcr.io/dgflagg-dms-www/dms-www:latest

# stop the currently running webserver container
docker rm -f dms-www

# run the webserver as a docker container
docker run --rm -dt --name dms-www \
    -p 80:3000 us.gcr.io/dgflagg-dms-api/dms-www:latest