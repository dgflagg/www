Frontend UI for Dead Man's Switch


# build docker image locally (no build cache)
docker build -t dms-www:local .

# run container
docker run --rm -dt --name dms-www -p 80:3000 dms-www:local

# interact from the browser
http://localhost:80
http://dms.dgflagg.info

TODO:
- implement other functions