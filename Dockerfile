# use an LTS version of node
FROM node:12.18-alpine3.12

# copy the source into the image
COPY app /dms-www

# set the work dir to the location of the source
WORKDIR /dms-www

# restrict the application permissions by creating and then running as a standard user
RUN addgroup -S dms-www && adduser -S dms-www -G dms-www

# make the www user the owner of the app directory
RUN chown -R dms-www:dms-www /dms-www

# change to the api user
USER dms-www:dms-www

# install npm dependencies
RUN npm install
RUN npm install axios

# run the application as the default entrypoint
ENTRYPOINT ["npm","start"]